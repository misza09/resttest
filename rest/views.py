# -*- coding: utf-8 -*-
from rest_framework import viewsets, mixins
from rest.models import Employee
from rest.serializers import EmployeeSerializer


class EmployeeViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.ListModelMixin,
                      mixins.DestroyModelMixin):
    """
    A simple ViewSet for listing or creating or destroying employees.
    """
    queryset = Employee.objects.all().order_by('surname', 'name')
    serializer_class = EmployeeSerializer

    def list(self, request, *args, **kwargs):
        params = request.query_params
        email = params.get('email', '')
        if email:
            self.queryset = self.queryset.filter(email__contains=email)

        return mixins.ListModelMixin.list(self, request, *args, **kwargs)
