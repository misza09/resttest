# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = '__all__'