# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from rest_framework import routers
from rest.views import EmployeeViewSet

router = routers.DefaultRouter()

router.register(r'employee', EmployeeViewSet)

urlpatterns = [url(r'^', include(router.urls)), ]
